import React, { Component, PropTypes } from 'react';
const profile = require('../assets/img/profile.png');

export default class Example extends Component {
    render() {
        return (
            <header>
                <div className="container">
                    <div className="row">
                        <div className="col-lg-12">
                            <img className="img-responsive" src={profile} alt="Profile image" />
                            <div className="intro-text">
                                <span className="name">React-Redux-Webpack Generator</span>
                                <hr className="star-light" />
                                <span className="skills">My generator, my rules...</span>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
        );
    }
}
