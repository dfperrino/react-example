import React, { Component, PropTypes } from 'react';
import { Navbar, Nav, NavItem, NavDropdown, MenuItem } from 'react-bootstrap';
import { Link } from 'react-router';

export default class Application extends Component {
    constructor(props) {
        super(props);
        this.handleScroll = this.handleScroll.bind(this);
    }

    /* We add the event listener for the scroll */
    componentDidMount() {
        window.addEventListener('scroll', this.handleScroll)
    }

    /* We remove the event listener for the scroll */
    componentWillUnmount() {
        window.removeEventListener('scroll', this.handleScroll);
    }

    /* The function that calls when the scroll event triggers */
    handleScroll(e) {
        const shrinkClass = 'navbar-shrink';
        const element = document.getElementById('top-navbar');
        let className = element.className;
        if (document.body.scrollTop > 100) {
            if (className.indexOf(shrinkClass) === -1) {
                className += ` ${shrinkClass}`;
            }
        } else {
            className = className.replace(shrinkClass, '');
        }
        element.className = className;
    }

    render() {
        return (
            <div style={{ height: '100%' }}>
                <Navbar id="top-navbar" fixedTop>
                    <Navbar.Header>
                        <Navbar.Brand>
                            <Link to="/">React-Redux-Webpack Generator</Link>
                        </Navbar.Brand>
                        <Navbar.Toggle />
                    </Navbar.Header>
                    <Navbar.Collapse>
                        <Nav>
                            <NavItem eventKey={1} href="#"><Link to="/redux">Redux</Link></NavItem>
                            <NavItem eventKey={2} href="#">Link</NavItem>
                            <NavDropdown eventKey={3} title="Dropdown" id="basic-nav-dropdown">
                                <MenuItem eventKey={3.1}>Action</MenuItem>
                                <MenuItem eventKey={3.2}>Another action</MenuItem>
                                <MenuItem eventKey={3.3}>Something else here</MenuItem>
                                <MenuItem divider />
                                <MenuItem eventKey={3.3}>Separated link</MenuItem>
                            </NavDropdown>
                        </Nav>
                    </Navbar.Collapse>
                </Navbar>

                {this.props.children}
            </div>
        );
    }
}
