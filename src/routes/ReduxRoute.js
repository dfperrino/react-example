import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as userActions from '../app/actions/user';
import { Alert } from 'react-bootstrap';

@connect(state => ({
    user: state.user
}), dispatch => ({
    userActions: bindActionCreators(userActions, dispatch)
}))
class ReduxRoute extends Component {
  render() {
      return (
          <header>
              <div className="container">
                  <div className="row">
                      <div className="col-lg-12">
                          {this.props.user.state === 'ERROR' &&
                            <Alert bsStyle="danger">ERROR!!!</Alert>
                          }
                          {this.props.user.state === 'SUCCESS' &&
                            <Alert bsStyle="success">{JSON.stringify(this.props.user, null, ' ')}</Alert>
                          }
                          {this.props.user.state === 'LOADING' &&
                            <Alert bsStyle="info"><i className='fa fa-circle-o-notch fa-spin'></i>&nbsp;Cargando...</Alert>
                          }
                          <a className="btn btn-default" onClick={this.props.userActions.getUserInfo}>Test me!</a>
                      </div>
                  </div>
              </div>
          </header>
      );
  }
}

export default ReduxRoute;
