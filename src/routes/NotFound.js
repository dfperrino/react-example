import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router';
const tumbleweed = require('../assets/img/tumbleweed.png');

export default class NotFound extends Component {
    render() {
        return (
            <div className="not-found-page">
                <div className="container">
                    <div className="col-xs-12 col-sm-7 col-lg-7">
                        <div className="info">
                            <h1>Oppps!</h1>
                            <h2>you have some troubles!</h2>
                            <p>The page you are looking for was moved, removed,
                                renamed or might never existed.</p>
                            <Link to="/" className="btn btn-default btn-lg">
                                <i className="fa fa-home" />&nbsp;Go Home
                            </Link>
                        </div>
                    </div>

                    <div className="col-xs-12 col-sm-5 col-lg-5 text-center">
                        <div className="fighting">
                            <img className="img-responsive" src={tumbleweed} />
                        </div>
                    </div>

                </div>
            </div>
        );
    }
}
