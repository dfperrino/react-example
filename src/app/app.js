import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import injectTapEventPlugin from 'react-tap-event-plugin';
import { Router, hashHistory } from 'react-router';
import routes from 'routes';
/** REDUX */
import logger from './util/logger';
import { Provider } from 'react-redux';
import { createStore, compose, applyMiddleware, bindActionCreators } from 'redux';
import reducers from './reducers/index';
import thunk from 'redux-thunk';
import { syncHistoryWithStore, routerMiddleware } from 'react-router-redux';
const store = compose(
    applyMiddleware(thunk, logger, routerMiddleware(hashHistory))
)(createStore)(reducers);

injectTapEventPlugin();
require('../assets/less/app.less');
require('../../node_modules/font-awesome/less/font-awesome.less');

class Root extends Component {
    render() {
        return (
            <div className="app-root" ref="appRoot">
                <Provider store={store}>
                    <Router history={hashHistory}>
                        {routes}
                    </Router>
                </Provider>
            </div>
        );
    }
}

ReactDOM.render(
    <Root />,
    document.getElementById('app')
);
