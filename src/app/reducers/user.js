import * as constants from '../constants';
import { Map } from 'immutable';

const defaultState = {
    username: null,
    nif: null,
    codAcreedor: null,
    state: '',
    stateMsg: '',
    stateMsgVal: []
};

export default function userInfoHandler(state = defaultState, action) {
    try {
        switch (action.type) {
        case constants.GET_USER_IS_FETCHING:
            return Map(state).merge({ state: 'LOADING' }).toJS();
        case constants.GET_USER_SUCCESS:
            return Map(state).merge(action.user).toJS();
        case constants.SET_USER_ID:
            return Map(state).merge({ codAcreedor: action.codAcreedor }).toJS();
        case constants.GET_USER_ERROR:
            return Map(state).merge({ state: 'ERROR', stateMsg: action.error, stateMsgVal: action.info || [] }).toJS();
        default:
            return state;
        }
    } catch (e) {
        return state;
    }
}
