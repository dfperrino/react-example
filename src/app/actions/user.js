import * as constants from '../constants';
import _ from 'lodash';

/**
 * Obtiene el usuario y los datos asociados
 * En caso de error, informa del mismo
 *
 * @returns Function dispatch
 */
export function getUserInfo() {
    return dispatch => {
        dispatch({ type: constants.GET_USER_IS_FETCHING });
        setTimeout(() => {
            const newUser = {
                username: 'dfernper',
                nif: '44913846K',
                codAcreedor: null,
                state: 'SUCCESS',
                stateMsg: '',
                stateMsgVal: []
            };
            dispatch({ type: constants.GET_USER_SUCCESS, user: newUser });
        }, 3000);
    };
}
