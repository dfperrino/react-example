import React from 'react';
import Application from '../routes/Application';
import Example from '../routes/Example';
import NotFound from '../routes/NotFound';
import ReduxRoute from '../routes/ReduxRoute';
import { Route, IndexRoute, Redirect } from 'react-router'

const routes = (
    <Route path="/" component={Application}>

        // We add the example home as a indexRoute
        <IndexRoute component={Example} />

        // Another route with Redux
        <Route path="/redux" component={ReduxRoute} />

        // Error page, which redirects to Home after showing an error
        <Route path="/error" component={NotFound} />

        // Every route not matching an existent one, will go to /error
        <Redirect from="*" to="/error" />
    </Route>
);

export default routes;
