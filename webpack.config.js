const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const Webpack = require('webpack');
const path = require('path');
const _package = require('./package.json');
const CopyWebpackPlugin = require('copy-webpack-plugin');

// base config properties
const baseConfig = {
    isProduction: true,
    environment: 'prod',
    tag: '-'.concat(_package.version),
    mockApi: false,
    dest: {
        fonts: 'assets/fonts/',
        imgs: 'assets/images/'
    },
    hashHistory: false
};

process.argv.forEach((val) => {
    switch (val) {
    case '--dev':
        baseConfig.environment = 'dev';
        baseConfig.isProduction = false;
        break;
    case '--mock':
        baseConfig.mockApi = true;
        break;
    case '--with-hash-history':
        baseConfig.hashHistory = true;
        break;
    default:
        break;
    }
});

console.log('** BUILDING -> '.concat(_package.name));
console.log('** TARGET -> '.concat((baseConfig.isProduction ? 'PRODUCTION' : 'DEVELOPMENT')));
console.log('** ENVIRONMENT -> '.concat(baseConfig.environment));
console.log('** MOCK SERVICES -> '.concat(baseConfig.mockApi));

const webpackConfig = {
    port: 8080, // The port for the webpack-dev-server
    context: path.join(__dirname, 'src'), // The context path for the application
    entry: { // The chunks in which the application will be divided
        app: ['./app/app.js'],
        vendor: ['react', 'react-router', 'lodash']
    },
    output: { // The build path
        path: path.join(__dirname, baseConfig.isProduction ? '/dist' : '/build'),
        filename: `app${baseConfig.tag}.bundle.js`
    },
    module: {
        loaders: [
            {
                test: /\.jsx?$/,
                exclude: /node_modules/,
                loader: 'babel',
                query: {
                    presets: ['react', 'es2015', 'stage-0'],
                    plugins: [
                        'transform-decorators-legacy'
                    ]
                }
            },
            {
                test: /\.(png|jpg|gif|svg)$/,
                loader: `url-loader?limit=50&name=${baseConfig.dest.imgs}[name].[ext]`
            },
            {
                test: /\.less$|\.css$/,
                loader: ExtractTextPlugin.extract('css!less')
            },
            {
                test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                loader: `url-loader?limit=1024&mimetype=application/font-woff&name=${baseConfig.dest.fonts}[name].[ext]`
            },
            {
                test: /\.(ttf|eot)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                loader: `file-loader?name=${baseConfig.dest.fonts}[name].[ext]`
            },
            {
                test: /\-(webfont\.svg|regular\.svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                loader: `file-loader?name=${baseConfig.dest.fonts}[name].[ext]`
            },
            {
                test: /\.json$/,
                loader: 'json'
            }
        ]
    },
    plugins: [
        new ExtractTextPlugin(`[name]${baseConfig.tag}.css`),
        new HtmlWebpackPlugin({
            template: './index.html',
            inject: false,
            hash: false
        }),
        new Webpack.DefinePlugin({
            'process.env': {
                NODE_ENV: baseConfig.environment === 'dev' ? JSON.stringify('development') : JSON.stringify('production')
            },
            __DEVTOOLS__: !baseConfig.isProduction,
            __MOCKAPI__: baseConfig.mockApi,
            __HASH_HISTORY__: baseConfig.hashHistory
        }),
        new Webpack.optimize.CommonsChunkPlugin({
            name: 'vendor',
            filename: `[name]${baseConfig.tag}.js`
        }),
        new CopyWebpackPlugin([{
            from: path.join(__dirname, '/.htaccess'),
            to: path.join(__dirname, baseConfig.isProduction ? '/dist' : '/build')
        }])
    ],
    resolve: {
        extensions: ['', '.js', '.jsx', '.less', '.css'],
        alias: {
            properties: path.join(__dirname, 'src/config/properties', baseConfig.environment),
            routes: path.join(__dirname, 'src/config/routes')
        }
    }
};
module.exports = webpackConfig;
