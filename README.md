react-example, 0.0.1
=========================================================

Proyecto de ejemplo para las charlas de arquitectura

Bajarse el proyecto y ejecutar "npm install" (Probado con node LTS y node 6.7.0. Usuarios de node 0.10.0 absténganse)
**START UP**
--------------------
Once we have our dependencies installed, we have several choices to run the application

- **Development server mode**:
This mode launches a development web server (there's no need to have an apache or nginx), that will execute the application code.
To start this mode, we should input:

> npm start

- **Mocked development server mode**:
Same as the development server mode, but using mocked responses (in src/api/rest-mock.js)
To start this mode, we should input:

> npm run server:mock

- **Deploy mode**:
This mode will generate a deployment package in the "dist" directory with minified, compressed and optimized code.
To start this mode, we should input:

> npm run dist

- **Test mode**:
This mode will launch the tests on the "test" directory
To start this mode, we should input:

> npm test

- **Test mode with hot-loader**:
Same as the test mode, but in this one, webpack will wait changes in the files, and it will relaunch the tests on save.
To start this mode, we should input:

> npm run test:watch
