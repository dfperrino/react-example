const webpack = require('webpack');
const WebpackDevServer = require('webpack-dev-server');
const config = require('./webpack.config');
const open = require('open');
const props = require('./src/config/base');
const proxies = require('./src/config/proxies');

const devServer = {
    proxy: proxies
};
config.entry.app.unshift(`webpack-dev-server/client?http://localhost:${config.port}/`);

new WebpackDevServer(webpack(config), devServer)
.listen(config.port, 'localhost', (err) => {
    if (err) {
        console.error(err);
    }
    console.log(`Listening at localhost: ${config.port}`);
    console.log('Opening your system browser...');
    open(`http://localhost:${config.port}/`);
});
